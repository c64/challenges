


# Scroller

Textscroller oder Laufschriften sind einer der essenziellen Effekte in
der Demo-Koderei. Die Bandbreite geht hier von einfachem von rechts
nach links laufendem text bis hin zu sogenannten SIN- (Sinus)
Scrollern, bei denen die Buchstaben ein einer Bewegungsrichtung hin
und her tanzen.

Eine schöne Einführung gibt es bei der [Codebase
64](https://codebase64.org/doku.php?id=base:text_scroll)

