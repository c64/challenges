;;; softscroll
	
;;;	If you want to do a smooth scroll two pixel steps or 1 pixel steps
;;;	are desirable. This is done using the soft-scroll register $d016
;;;	(for a detailed description look: https://sh.scs-trc.net/vic/). If
;;;	you want to scroll left you start with $x7 in $d016 and decrease
;;;	that value every frame. After 8 times you reset this register to its
;;;	start value and move all characters one byte backwards using the
;;;	according one of above scroll routines.
	
;;; https://codebase64.org/doku.php?id=base:text_scroll

	
	!cpu 6510
	!to "leftright.prg",cbm

  *= $c000
 
Q        = 2
XPIXSHIFT = 4
TMP1     = 5
TEXTADR  = 6
 
SCRLADR  = $0400
 
  JSR $E544
 
  SEI
TEXTRESTART
  LDA #<TEXT
  STA TEXTADR
  LDA #>TEXT
  STA TEXTADR+1
 
LOOP     INC $d012
  BNE LOOP
 
DESTSTART = *+1
  LDX #39;39
SRCSTART = *+1
  LDY #39;37
 
XPIXSHIFTADD
  DEC XPIXSHIFT
	
  LDA XPIXSHIFT
  AND #7
  STA $D016
 
  CMP XPIXSHIFT
  STA XPIXSHIFT
  BEQ LOOP
 
  LDA SCRLADR,Y
  STA TMP1
  LDA SCRLADR-1,Y
  PHA
S
  LDA TMP1
  STA SCRLADR-1,X
  PLA
  STA TMP1
  LDA SCRLADR-2,Y
  PHA
  DEY
  DEX
  BNE S
  PLA
GETNEWCHAR
;TEXTADR  = *+1
  LDA (TEXTADR,X)
  BEQ TEXTRESTART
 
  INY
  BMI *+4
  LDX #$27
 
NOBEGIN  INC TEXTADR
  BNE *+4
  INC TEXTADR+1
 
  TAY
  BMI DIRCHANGE
 
  STA SCRLADR,X
  BPL LOOP
;---------------------------------------
DIRCHANGE LDA XPIXSHIFTADD
  EOR #$20
  STA XPIXSHIFTADD
 
  LDX DESTSTART
  LDY SRCSTART
  DEX
  INY
  STX SRCSTART
  STY DESTSTART
  BNE LOOP										; $106b
;---------------------------------------
TEXT     !scr " this scroller can"
         !scr " scroll in forward"
         !scr " and backward direc"
         !scr "tion!               "
         !scr "                    "
         !scr "         "
         !byte $ff
         !scr "won gnillorcs morf "
         !scr "tfel ot thgir ...   "
         !scr "                    "
         !scr "                    "
         !byte $ff,0
