

;test2.prg ==0801==
   10 rem annettes laufschrift
   20 rem 2019-05-27
 1000 rem hauptprogramm
 1100 rem initialisierung
 1110 gosub 2110 : rem bildschirm loeschen
 1120 gosub 4010 : rem eingabe laufschrift
 1130 gosub 5010 : rem pause
 1140 gosub 2110 : rem bildschirm loeschen
 1150 xp=10 : rem start-x-position
 1160 yp=14 : rem start-y-position
 1170 xm=42 : rem max-x-position
 1180 ym=25 : rem max-y-position
 1190 dim po(ll) : rem laenge laufschrift
 1600 rem -=- hauptprogramm -=-
 1610 gosub 2220 : rem schrift ausgeben
 1620 gosub 5110 : rem cursor zurueck
 1630 gosub 5210 : rem ueberdrucken
 1999 end
 2000 rem bildschirmausgaben
 2100 rem bildschirm loeschen
 2110 print chr$(147);
 2120 return
 2200 rem - schrift ausgeben -
 2210 rem positionierung des cursors
 2220 print chr$(19); : rem home
 2230 for x=1 to xp : print chr$(17);: next : rem nach unten
 2240 for y=1 to yp : print chr$(29);: next : rem nach rechts
 2250 print ls$; : rem laufschrift drucken
 2260 gosub 5010 : rem pause
 2270 gosub 5110 : rem cursor zurueck
 2280 gosub 5210 : rem ueberdrucken
 2290 gosub 3110 : rem tastaturabfrage
 2300 goto2200
 2999 end
 3000 rem steuerungsinput
 3100 rem tastaturabfrage
 3110 a$=""
 3120 get a$
 3130 if a$="" then goto 3120
 3140 rem print "&";a$;"&"
 3150 if a$="a" then yp=yp-1
 3160 if a$="d" then yp=yp+1
 3170 if a$="w" then xp=xp-1
 3180 if a$="s" then xp=xp+1
 3200 return
 4000 rem stringeingabe fuer laufschrift
 4010 print
 4015 print "gib die gewuenschte laufschrift ein."
 4020 input ls$
 4030 lls=len(ls$)
 4040 dim q$(lls):for i=1 to lls : q$(i)=" " : next
 4050 dim r$(lls): for i=1 to lls : r$(i)=chr$(157) : next :rem links
 4060 print ls$;" ";lls
 4070 return
 5000 rem pause
 5010 for pause=1 to 50 : next
 5020 return
 5100 rem -cursor zurueck-
 5110 for i=1 to lls : print r$(i); : next
 5120 return
 5200 rem -laufschrift leer ueberdrucken-
 5210 for i=1 to lls : print q$(i); : next
 5220 return

