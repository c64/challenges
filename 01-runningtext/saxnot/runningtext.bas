0000 rem title   saxnots erster textscroller
0000 rem author  saxnot
0000 rem date    2019-06-04

0000 rem bildschirm verkleinert (von 40 auf 38 cols)
0000 rem dies ist notwendig um smooth zu scrollen
0100 poke 53270,peek(53270) and 247

0000 rem "clear home" character ausgeben
0200 printchr$(147)

0000 rem variablen für laufschrift
0300 n% = 40
0400 tx$ = "                                      saxnot"

0000 rem das linkste char in jedem durchlauf wegwerfen
0500 tx$ = right$(tx$,n%)

0000 rem "clear home" character zum leeren des bildschirms
0700 print chr$(147)

0000 rem scroller startet bei 7 offset
0800 poke 53270, (peek(53270) and 248) + 7

0900 print tx$

0000 rem den angezeigten bildschirm von offet 7 bis 0 smooth verschieben
1000 for p=7 to 0 step -1
   1100 poke 53270, (peek(53270) and 248) + p
1200 next p

0000 rem laenge des strings verringern und wieder von vorn
1300 n% = n% - 1
1400 if n% > 0 then goto 0500

0000 rem wenn ganz links angekommen beginne von vorn
1500 goto 0300
