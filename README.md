


# Commondore64 Programme

Wir lernen C64 Assembler! Basic(s) first!!



## Hausaufgaben

Die Aufgaben sind in den README Dateien in den jeweiligen
Verzeichnissen umrissen. Da die Einarbeitung in einige Themen
allerdings auch etwas länger dauern kann, sind diese als
Dauerherausforderungen zu verstehen.

* Basic Text Scroller (zum 2019-06-04)
* Joystick Input (zum 2019-06-18)
* Musik/Ton (zum ?)
* Grafik/PETSCII Art



## Standards

Kein Leben ohne Struktur! Deshalb versuchen wir die folgenden
grundlegenden Codings-Styles einzuhalten:

 1. Assembler-Beispiele starten bei $c000 (sys 49152) und als standard
 assembler wird ACME verwendet.

 2. BASIC wird klein geschrieben damit es mit 'petcat' ohne probleme
 übersetzt werden kann.



## Befehle

Zum Kompilieren von BASIC:
```petcat -2 -w2 -o programm-name.prg quelltext.bas```

Zum Kompiliern von Assembler:
```acme quelltext.asm```

Und für den Fall, dass im Quelltext nicht schon die entsprechenden
parameter für cpu, ausgabedatei und format gesetzt sind:
```acme -f cbm --cpu 6510 -o programm-name.prg quelltext.asm```

Getestet können die so entstandenen Programme direkt mit 'x64' werden:
```x64 programm-name.prg```

Bei Assembler/Maschinensprache muss nach dem laden das Programm durch
Eingabe von ```sys 49152``` also die vereinbarte Basisadresse
gestartet werden.

