
  ;;
  ;; Assembly Language for Kids - Page 243
  ;;

  ;; The program draws low resolution lines on the screen. To keep it
  ;; simple and a little more flexible, we'll use the joystick LEFT to serve
  ;; as an eraser. Therefore, if you move the joystick UP, DOWN or
  ;; RIGHT, a line will be drawn. Move it left, and anything the cursor
  ;; hits will be erased.

  ;; ACME Assembler
  !to "crudedraw.prg", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

	clear  = $E544
	jstick = $DC00
	offset = $C200
	fire   = $C202
	invrse = $C204
	normal = $C206
	mark   = $C208
	chrout = $FFD2

	jsr clear
	lda #$FF
	sta offset
	lda #$10
	sta fire
	lda #$20
	sta mark
	lda #$12
	sta invrse
	lda #$92
	sta normal

start:	lda jstick
	eor offset
	cmp #$01
	beq up
  cmp #$02                      ; Joystick down?
  beq down
  cmp #$04                      ; Joystick left?
  beq left
  cmp #$08                      ; Joystick right?
  beq right

  cmp fire                      ; Fire button pressed?
  beq end                         ; If so then end

cursor:   lda mark                ; Load the space
  jsr chrout                    ; Print the space
  lda #$9D                      ; Load the left cursor
  jsr chrout                    ; Back up
  lda normal
  jsr chrout                    ; Set normal
  lda mark
  jsr chrout
  lda #$9D
  jsr chrout
  lda invrse
  jsr chrout
	ldx #$00											; Begin pause loop

pause:	inx
	cpx #$FE
	bne pause											; End pause loop
	jmp start

up:	jsr back
	lda #$91
	jmp print

down:	jsr back
	lda #$11
	jmp print

left:	lda #$9D
	jsr chrout
	jmp print

right: lda #$1D

print:	jsr chrout
	lda mark
	jsr	chrout
	jmp cursor

back:	lda #$9D									; Subroutine
	jsr chrout
	rts

end:	rts
