1 rem  
2 rem https://www.c64brain.com/devices/joystick-controller-for-commodore/
3 rem
5 print chr$(147)
8 x1=12 :y1=4 :co=0
10 fi=(peek(56320)and 16)/16
15 jo=not peek(56320)and 15
20 if jo=1 then gosub 210
25 if jo>0 then poke 53280,jo+(fi+2)
30 if jo=2 then gosub 240
40 if jo=4 then gosub 270
50 if jo=8 then gosub 300
60 :
70 poke 214,y1 :print :poke 211,x1 :poke 646,co :print chr$(65+int(co));
80 if fi<1 then co=1+rnd(0)*15
100 goto 10
200 rem move up
210 y1=y1-1:ify1<1theny1=19 
220 return 
230 rem move down 
240 y1=y1+1:ify1>19theny1=1
250 return
260 rem move left
270 x1=x1-1:ifx1<1thenx1=39 
280 return 
290 rem move right 
300 x1=x1+1:ifx1>39thenx1=1
310 return
