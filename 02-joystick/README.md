


# Joystick

Die eingaben des Joysticks abfangen und auswerten. Im Prinzip wie
alles beim C64er ganz einfach: Man muss nur das Register $dc00 für
Port-1 und $dc01 für Port-2 auslesen... Aber dann hat man leider
erstmal nur einen recht unschönen 'POC' zum entprellen der Schalter
und für sanfte bewegungen braucht es dann doch noch etwas mehr
Engagement.

Wieder eine Einführung bei der [Codebase
64](https://codebase64.org/doku.php?id=base:joystick_input_handling)
und im
[Referenzhandbuch](https://gitli.stratum0.org/c64/cbm-c64prg_epub/blob/master/CBMC64PRG_06.md#06-4)


**Pro-Tip**: Wenn irgend etwas nicht auf Anhieb funktioniert liegt es
  meist daran, dass der Joystick am falschen Port ist, bzw. das
  Programm am falschen Port guckt.
